# ¡Bienvenidos a Secure Ride! 🎉

¡Bienvenidos al repositorio oficial de Secure Ride! Este proyecto es el resultado de un arduo trabajo y dedicación en el marco de mi proyecto de titulación. Aquí, todos están invitados a explorar y aprender de esta experiencia educativa, donde se desarrolló Secure Ride utilizando Flutter y Dart en un entorno Linux.

## Agradecimientos 🙏

Quiero expresar mi más sincero agradecimiento a todos aquellos que me apoyaron y guiaron a lo largo de mi formación. Cada conocimiento adquirido ha sido invaluable en este viaje, y estoy profundamente agradecido por la oportunidad de aprender y crecer.

## Recursos

Este proyecto sirve como un punto de partida para el desarrollo de aplicaciones Flutter. Ya sea que estés comenzando con Flutter o ya tengas experiencia, aquí tienes algunos recursos para ayudarte:

- [Laboratorio: Escribe tu primera aplicación Flutter](https://docs.flutter.dev/get-started/codelab)
- [Recetario: Ejemplos útiles de Flutter](https://docs.flutter.dev/cookbook)

Para obtener ayuda adicional con el desarrollo de Flutter, consulta la [documentación en línea](https://docs.flutter.dev/), que ofrece tutoriales, ejemplos, orientación sobre desarrollo móvil y una completa referencia de API.

## Dependencias Principales

Estas son las principales dependencias utilizadas en el proyecto Secure Ride:

- **cloud_firestore:** ^4.15.3
- **cupertino_icons:** ^1.0.2
- **firebase_auth:** ^4.16.0
- **firebase_core:** ^2.24.2
- **flutter_custom_clippers:** ^2.1.0
- **progress_dialog_null_safe:** ^2.0.0

Para ver más detalles sobre las dependencias y cómo están configuradas, consulta el archivo `pubspec.yaml` del proyecto.

## Nota

En la fase actual del proyecto, se utilizan imágenes obtenidas de Internet con fines educativos para proporcionar una representación visual de las características previstas de la aplicación. Sin embargo, es importante destacar que estamos trabajando activamente con un diseñador para crear nuestras propias ilustraciones exclusivas que reflejen la identidad y visión de Secure Ride.

