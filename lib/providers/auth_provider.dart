import 'package:firebase_auth/firebase_auth.dart';

// correo: na796840@gmail.com
// password: nicolas123
class AuthProvider {
  late FirebaseAuth _firebaseAuth;

  AuthProvider() {
    _firebaseAuth = FirebaseAuth.instance;
  }

  User? getUser(){
    return _firebaseAuth.currentUser;
  }

  Future<bool> login(String email, String password) async {
    String? errorMessage;

    try {
      await _firebaseAuth.signInWithEmailAndPassword(email: email, password: password);
    } catch (error) {
      print(error);
      // Correo Invalido
      // Password Incorrecto
      // No hay Conexion a Internet
      errorMessage = (error as FirebaseAuthException).code;
    }
    if (errorMessage != null) {
      return Future.error(errorMessage);
    }
    return true;
  }

  Future<bool> register(String email, String password) async {
    String? errorMessage;

    try {
      await _firebaseAuth.createUserWithEmailAndPassword(email: email, password: password);
    } catch (error) {
      print(error);
      // Correo Invalido
      // Password Incorrecto
      // No hay Conexion a Internet
      errorMessage = (error as FirebaseAuthException).code;
    }
    if (errorMessage != null) {
      return Future.error(errorMessage);
    }
    return true;
  }
}
