import 'package:flutter/material.dart';

class Colors {

  static const Color primaryColor = Color(0xFFFFFFFF);  //! White

  static const Color secondaryColor = Color(0xFF222327);  //! Black

  static const Color tertiaryColor = Color(0x000000ff);  //! Por Asignar

  static const Color quaternaryColor = Color(0x000000ff);  //! Por Asignar

  static const Color quinaryColor = Color(0x000000ff);  //! Por Asignar

  static const Color senaryColor = Color(0x000000ff);  //! Por Asignar

  static const Color septenaryColor = Color(0x000000ff);  //! Por Asignar

  static const Color octonaryColor = Color(0x000000ff);  //! Por Asignar

  static const Color nonaryColor = Color(0x000000ff);  //! Por Asignar

  static const Color denaryColor = Color(0x000000ff);  //! Por Asignar

}