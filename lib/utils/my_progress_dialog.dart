import 'package:flutter/material.dart';
import 'package:progress_dialog_null_safe/progress_dialog_null_safe.dart';
import 'package:secure_ride/utils/colors.dart' as custom_colors;

class MyProgressDialog {

  static ProgressDialog createProgressDialog(BuildContext context, String text) {

    ProgressDialog progressDialog = ProgressDialog(
      context,
      type: ProgressDialogType.normal,
      isDismissible: false, 
      showLogs: false
    );

    progressDialog.style(
      message: text,
      borderRadius: 10.0,
      backgroundColor: custom_colors.Colors.primaryColor,
      progressWidget: Container(
        padding: const EdgeInsets.all(10.0),
        child: const CircularProgressIndicator()),
      elevation: 10.0,
      insetAnimCurve: Curves.easeInOut,
      progress: 0.0,
      maxProgress: 100.0, 
      progressTextStyle: const TextStyle(
        color: custom_colors.Colors.secondaryColor, 
        fontSize: 12.0, 
        fontWeight: FontWeight.w400
      ),
      messageTextStyle: const TextStyle(
        color: custom_colors.Colors.secondaryColor, 
        fontSize: 17.0, 
        fontWeight: FontWeight.w600
      )
    );
            
    return progressDialog;
  }
}
