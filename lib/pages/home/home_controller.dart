import 'package:flutter/material.dart';

class HomeController {
   static goToLoginPage(BuildContext context) {
    Navigator.pushNamed(context, "login");
  }
}
