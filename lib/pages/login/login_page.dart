import 'package:secure_ride/pages/login/login_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:secure_ride/utils/colors.dart' as custom_colors;

import '../../widgets/button_app.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  late LoginController loginController;

  @override
  void initState() {
    super.initState();
    loginController = LoginController();
    initLoginController();
  }

  Future<void> initLoginController() async {
    print("Init State");
    await loginController.init(context);
    print("Metodo Schedule");
  }

  @override
  Widget build(BuildContext context) {
    print("Metodo Build");
    return Scaffold(
      key: loginController.key,
      appBar: AppBar(
        backgroundColor: custom_colors.Colors.secondaryColor,
      ),
      body:  SingleChildScrollView(
        child: Column(
          children: [
            const _BannerApp(),
            const SizedBox(height: 10),
            const _TextDescription(),
            const _TextLogin(),
            const SizedBox(height: 30),
            _TextFieldEmail(loginController: loginController,),
            const SizedBox(height: 20),
            _TextFieldPassword(loginController: loginController,),
            const SizedBox(height: 40),
            _ButtonLogin(loginController: loginController,),
            const SizedBox(height: 20),
            const _TextDontHaveAccount(),
          ],
        ),
      )
    );
  }
}

class _BannerApp extends StatelessWidget {
  const _BannerApp();

  @override
  Widget build(BuildContext context) {
    return ClipPath(
      clipper: WaveClipperTwo(),
      child: Container(
        color: custom_colors.Colors.secondaryColor,
        height: MediaQuery.of(context).size.height * 0.22,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Image.asset(
              "assets/img/logo_app.png",
              width: 150,
              height: 100,
            ),
            const Text(
              "Facil y Rapido",
              style: TextStyle(fontFamily: "Pacifico", fontSize: 22, color: custom_colors.Colors.primaryColor, fontWeight: FontWeight.bold),
            )
          ],
        ),
      ),
    );
  }
}

class _TextDescription extends StatelessWidget {
  const _TextDescription();

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerLeft,
      margin: const EdgeInsets.symmetric(horizontal: 30, vertical: 10),
      child: const Text("Continua con tu",
      style: TextStyle(
        color: custom_colors.Colors.secondaryColor,
        fontSize: 24,
        fontFamily: "NimbusSans"
      ),
      ),
    );
  }
}

class _TextLogin extends StatelessWidget {
  const _TextLogin();

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerLeft,
      margin: const EdgeInsets.symmetric(horizontal: 30),
      child: const Text("Login",
      style: TextStyle(
        color: custom_colors.Colors.secondaryColor,
        fontWeight: FontWeight.bold,
        fontSize: 28
      ),),
    );
  }
}

class _TextFieldEmail extends StatelessWidget {
  final LoginController loginController;
  const _TextFieldEmail({
    required this.loginController,
    
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 30),
      child:  TextField(
        controller: loginController.emailController,
        style: const TextStyle(color: custom_colors.Colors.secondaryColor),
        decoration: const InputDecoration(
          hintText: "correo@gmail.com",
          hintStyle: TextStyle(color: custom_colors.Colors.secondaryColor),
          labelText: "Correo electronico",
          labelStyle: TextStyle(color: custom_colors.Colors.secondaryColor),
          suffixIcon: Icon(Icons.email_outlined,
          color: custom_colors.Colors.secondaryColor,
          )
        ),
      ),
    );
  }
}

class _TextFieldPassword extends StatelessWidget {
  final LoginController loginController;
  const _TextFieldPassword({required this.loginController});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 30),
      child: TextField(
        controller: loginController.passwordController,
        obscureText: true,
        style: const TextStyle(color: custom_colors.Colors.secondaryColor),
        decoration: const InputDecoration(
          labelText: "Contraseña",
          labelStyle: TextStyle(color: custom_colors.Colors.secondaryColor),
          suffixIcon: Icon(Icons.lock_open_outlined,
          color: custom_colors.Colors.secondaryColor,
          )
        ),
      ),
    );
  }
}

class _ButtonLogin extends StatelessWidget {
  final LoginController loginController;
  const _ButtonLogin({required this.loginController});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 30, vertical: 25),
      child: ButtonApp(
        onPressed: loginController.login, 
        text: 'Iniciar Sesion',),
    );
  }
}

class _TextDontHaveAccount extends StatelessWidget {
  const _TextDontHaveAccount();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        LoginController.goToRegisterPage(context);
      },
      child: Container(
        margin: const EdgeInsets.only(bottom: 50),
        child: const Text(
          "¿No tienes cuenta?",
          style: TextStyle(
            fontSize: 15,
            color: Colors.grey
          ),
        ),
      ),
    );
  }
}
