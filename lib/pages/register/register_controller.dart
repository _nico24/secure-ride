import 'package:flutter/material.dart';
import 'package:progress_dialog_null_safe/progress_dialog_null_safe.dart';
import 'package:secure_ride/models/client.dart';
import 'package:secure_ride/providers/auth_provider.dart';
import 'package:secure_ride/providers/client_provider.dart';
import 'package:secure_ride/utils/my_progress_dialog.dart';
import 'package:secure_ride/utils/snackbar.dart' as utils;


class RegisterController {
  late BuildContext context;
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();

  late TextEditingController userNameController = TextEditingController();
  late TextEditingController emailController = TextEditingController();
  late TextEditingController passwordController = TextEditingController();
  late TextEditingController confirmPasswordController = TextEditingController();

  late AuthProvider authProvider = AuthProvider();
  late ClientProvider clientProvider;
  late ProgressDialog progressDialog;

  Future<void> init(BuildContext context) async {
    this.context = context;
    authProvider = AuthProvider();
    clientProvider = ClientProvider();
    progressDialog = MyProgressDialog.createProgressDialog(context, "Espere un momento...");
  }

  Future<void> register() async {
    String username = userNameController.text;
    String email = emailController.text.trim();
    String password = passwordController.text.trim();
    String confirmpassword = confirmPasswordController.text.trim();

    print("Username: $username");
    print("Email: $email");
    print("Password: $password");
    print("Confirm Password: $confirmpassword");


    if (username.isEmpty && email.isEmpty && password.isEmpty && confirmpassword.isEmpty) {
      print("Debes ingresar todos los campos");
      utils.Snackbar.showSnackbar(context, key, "Debes ingresar todos los campos");
      return;
    }

    if (confirmpassword != password) {
      print("las contraseñas no coinciden");
      utils.Snackbar.showSnackbar(context, key, "Las contraseñas no coinciden");
      return;
    }

    if (password.length < 6) {
      print("La contraseña debe tener almenos 6 caracteres");
      utils.Snackbar.showSnackbar(context, key, "La contraseña debe tener almenos 6 caracteres");
      return;
    }

    progressDialog.show();

    try {
      bool isRegister = await authProvider.register(email, password);

      if (isRegister) {

        Client client = Client(
          id: authProvider.getUser()?.uid ?? "", 
          email: authProvider.getUser()?.email ?? "", 
          username: username, 
          password: password);

        await clientProvider.create(client);

        progressDialog.hide();
        utils.Snackbar.showSnackbar(context, key, "El usuario se registro correctamente");
        print("El usuario se registro correctamente");
      } else {
        progressDialog.hide();
        utils.Snackbar.showSnackbar(context, key, "El usuario no se pudo registrar");
        print("El usuario no se pudo registrar");
      }

    } catch (error) {
        progressDialog.hide();
        utils.Snackbar.showSnackbar(context, key, "Error: $error");
      print("Error: $error");
    }
    
  }
}
